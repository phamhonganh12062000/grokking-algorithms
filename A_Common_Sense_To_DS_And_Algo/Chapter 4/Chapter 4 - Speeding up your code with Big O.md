
> [!summary] 3-sentence summary
> - In bubble sort, we compare the two values in an array and swapping their positions if they are out of order 
> - Bubble sort is not really efficient with $O(N^2)$ and especially when the array is already sorted.


> [!info] The point of reseraching sorting algorithms
> 
> To answer the question: *Given an array of unsorted values, how can we sort them so that they end up in ascending order?*


[[Bubble sort]]

[[A quadratic problem]]

[[Speeding up your code with Big O - Exercises]]

