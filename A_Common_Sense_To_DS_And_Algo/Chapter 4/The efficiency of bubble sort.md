---
tags:
  - "#study"
  - "#review"
cssclasses:
  - center-images
---
- Steps: $O(n^2)$ - Quadratic time
	- *Comparisons* takes $(n-1) + (n-2) + \dots + 1$ comparisons with $n$ elements
	- *Swaps*
		- Worst-case scenario:  Array is sorted in descending order => 1 comparison goes with 1 swap