---
tags:
  - "#study"
  - "#review"
cssclasses:
  - center-images
sr-due: 2024-11-10
sr-interval: 35
sr-ease: 270
---

> [!summary] 3-sentence summary
> - Data structures are the ways data is organized and each data structure possesses four basic operations: Reading/Searching/Insertion/Deletion
> - Arrays are good at reading and searching, but they are not efficient at insertion and deletion as they have to move all of their elements to make space.
> - Sets do not allow duplicate values.

Several measures of code quality:

- **Maintainability** - Aspects: Readability, organization, and modularity
- **Efficiency** - One code piece running faster than the other


[[What are data structures]]

[[The Array]]

[[What are sets]]

[[Why DS Matter - Exercises]]

