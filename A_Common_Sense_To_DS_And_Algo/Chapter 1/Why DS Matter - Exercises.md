---
tags:
  - "#study"
cssclasses:
  - center-images
---
## Ex1

a. $O(1)$
b. $O(100)$
c. $O(101)$
d. $O(1)$
e. $O(101)$
f. $O(1)$

## Ex2

a. $O(1)$
b. $O(100)$
c. $O(201)$
d. $O(101)$
e. $O(201)$
f. $O(101)$


## Ex 3

$n$ steps to find all instances of the word "apples"