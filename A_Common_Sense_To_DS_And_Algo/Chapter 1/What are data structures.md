---
tags:
  - "#study"
cssclasses:
  - center-images
---
- Refer how **data is organized**
- The data structures you select may affect whether your software runs at all, or simply conks out because it can’t handle the load.