---
tags:
  - "#study"
  - "#review"
cssclasses:
  - center-images
---
- The computer has the ability to **jump to any particular index in the array and peer inside**
- When a program declares an array, it **allocates a contiguous (next to each other) set of empty cells for use in the program**

![[Pasted image 20231227214022.png]]


- Every cell in a computer's memory has a specific address, and each cell's memory address is **one number greater than the previous cell's address**

![[Pasted image 20231227214357.png]]


- Steps to take: 1 step in $O(1)$

> [!info] Why the computer can jump straight at an index
> 1. A computer can jump to **any** memory address **in one step** ($O(1)$)
> 2. The computer **makes note at which memory address the array begins** => The computer can jump to the appropriate memory address to find the value
