---
tags:
  - "#study"
cssclasses:
  - center-images
---

When we measure how “fast” an operation takes, we do not refer to how fast the operation takes in terms of pure time, but instead in **how many steps it takes**.

Four operations: 
- [[Array - Read Operation]] 
- [[Array - Search Operation ]]
- [[Array - Insert Operation]]
- [[Array - Delete Operation]]