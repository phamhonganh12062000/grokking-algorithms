
> [!summary] 3-sentence summary
> - Recursion allows the method to call itself until it reaches the base case, in which the function calls in the call stack will then be executed.
> - The returned value from the base case will be added up with the returned values from function calls from the stack.
> - Recursion is not a silver bullet to solve every problem.


[[Recurse Instead of Loop]]

[[Recursion in the eyes of the computer]]

[[Filesystem traversal]]

—

## Exercises


1. `low > high`
2. Infinitive recursion
