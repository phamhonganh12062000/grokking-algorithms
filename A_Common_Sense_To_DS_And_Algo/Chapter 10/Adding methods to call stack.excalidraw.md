---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
factorial(3) ^J90bHaLC

factorial(2) ^uMz60HX6

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/2.0.18",
	"elements": [
		{
			"type": "rectangle",
			"version": 53,
			"versionNonce": 206669281,
			"isDeleted": false,
			"id": "hyzsgAPyNCtA0Sapdxhd4",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -139.25,
			"y": -231.0859375,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 147,
			"height": 36,
			"seed": 2018385249,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [],
			"updated": 1711955394875,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 103,
			"versionNonce": 1782449441,
			"isDeleted": false,
			"id": "J90bHaLC",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -121.25,
			"y": -184.0859375,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 115.73989868164062,
			"height": 25,
			"seed": 109273871,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1711955813821,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "factorial(3)",
			"rawText": "factorial(3)",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "factorial(3)",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "arrow",
			"version": 109,
			"versionNonce": 1940729057,
			"isDeleted": false,
			"id": "v_gHAScJoi2f5HxW502l-",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -67.25,
			"y": -323.5859375,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 2.751316140972662,
			"height": 92.99999999999997,
			"seed": 110401231,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1711955816255,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": {
				"elementId": "uMz60HX6",
				"focus": -0.07921814814522911,
				"gap": 7.000000000000028
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-2.751316140972662,
					92.99999999999997
				]
			]
		},
		{
			"type": "rectangle",
			"version": 89,
			"versionNonce": 921126735,
			"isDeleted": false,
			"id": "4_m20Wf2_dmWzSpiZ6c-s",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -139.75,
			"y": -192.0859375,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 147,
			"height": 36,
			"seed": 1892480417,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [],
			"updated": 1711955566986,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 152,
			"versionNonce": 1846995137,
			"isDeleted": false,
			"id": "uMz60HX6",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -124.11994934082031,
			"y": -223.5859375,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 116.35989379882812,
			"height": 25,
			"seed": 697149391,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "v_gHAScJoi2f5HxW502l-",
					"type": "arrow"
				}
			],
			"updated": 1711955816255,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "factorial(2)",
			"rawText": "factorial(2)",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "factorial(2)",
			"lineHeight": 1.25,
			"baseline": 18
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "solid",
		"currentItemStrokeWidth": 2,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 255.125,
		"scrollY": 417.6027173200995,
		"zoom": {
			"value": 2
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%