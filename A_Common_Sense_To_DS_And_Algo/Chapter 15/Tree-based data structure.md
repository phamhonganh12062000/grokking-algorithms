---
tags:
  - "#study"
  - "#review"
  - "#programming"
  - "#algorithm"
cssclasses:
  - center-images
---

> [!info] Tree
> 
> A **tree** is a node-based data structure in which each node can have links to **multiple** nodes (similar to a linked list)

![[Pasted image 20240608154835.png]]


Structure:
- Uppermost node is root
- A node has a parent/child which is another node
- A node can have descendants (All nodes stemming from a node) and ancestors (All nodes that a node stems from)
- Trees have **levels** (row index within a tree)
- A tree is balanced when *its nodes’ sub-trees have the same number of nodes in it*