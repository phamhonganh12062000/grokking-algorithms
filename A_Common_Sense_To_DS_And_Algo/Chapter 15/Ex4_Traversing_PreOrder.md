---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
5 ^3UD5h5xz

3 ^qdEAmFt5

8 ^dzVhTMWV

2 ^PbqyyFEh

4 ^L8pmjcF5

7 ^23kg61od

9 ^6AEHeNyA

5 -> 3 -> 2 -> 4 -> 8 -> 7 -> 9 ^BCirn9MW

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/2.0.18",
	"elements": [
		{
			"id": "qb3SZ-00U1aodyWE9zShi",
			"type": "ellipse",
			"x": -73.050048828125,
			"y": -284.78125,
			"width": 66.4000244140625,
			"height": 65.59999084472656,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1081808801,
			"version": 78,
			"versionNonce": 802610927,
			"isDeleted": false,
			"boundElements": [
				{
					"id": "u-UJBVuhZFL-qyIPzjJiD",
					"type": "arrow"
				},
				{
					"id": "tvu5Mfx0G-PUfZCFjXq44",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "3UD5h5xz"
				}
			],
			"updated": 1718814175319,
			"link": null,
			"locked": false
		},
		{
			"id": "3UD5h5xz",
			"type": "text",
			"x": -46.005983063942956,
			"y": -264.6743537636775,
			"width": 12.3599853515625,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 280416193,
			"version": 5,
			"versionNonce": 916138255,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1718814175319,
			"link": null,
			"locked": false,
			"text": "5",
			"rawText": "5",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 18,
			"containerId": "qb3SZ-00U1aodyWE9zShi",
			"originalText": "5",
			"lineHeight": 1.25
		},
		{
			"type": "ellipse",
			"version": 108,
			"versionNonce": 255740513,
			"isDeleted": false,
			"id": "RYaD8_CkxvQXkdHmx9q1Y",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -157.45001220703125,
			"y": -191.18123626708984,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 66.4000244140625,
			"height": 65.59999084472656,
			"seed": 1310689871,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "u-UJBVuhZFL-qyIPzjJiD",
					"type": "arrow"
				},
				{
					"id": "NdlNXX6pnJ-7LDU3KwCTp",
					"type": "arrow"
				},
				{
					"id": "zxg9_4lfpwpA7q-Z2t7TA",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "qdEAmFt5"
				}
			],
			"updated": 1718814144499,
			"link": null,
			"locked": false
		},
		{
			"id": "qdEAmFt5",
			"type": "text",
			"x": -131.0359513256617,
			"y": -171.07434003076736,
			"width": 13.6199951171875,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1140056591,
			"version": 4,
			"versionNonce": 371823073,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1718814145878,
			"link": null,
			"locked": false,
			"text": "3",
			"rawText": "3",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 18,
			"containerId": "RYaD8_CkxvQXkdHmx9q1Y",
			"originalText": "3",
			"lineHeight": 1.25
		},
		{
			"type": "ellipse",
			"version": 139,
			"versionNonce": 2091047343,
			"isDeleted": false,
			"id": "VmuEOByQ50QaRsYj2ibwT",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 23.3499755859375,
			"y": -195.18126678466797,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 66.4000244140625,
			"height": 65.59999084472656,
			"seed": 1222144577,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "tvu5Mfx0G-PUfZCFjXq44",
					"type": "arrow"
				},
				{
					"id": "Fcfm6ZBVMkoq-5wmebq1D",
					"type": "arrow"
				},
				{
					"id": "42u8rb6R6VPbXqmfvmgsE",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "dzVhTMWV"
				}
			],
			"updated": 1718814147403,
			"link": null,
			"locked": false
		},
		{
			"id": "dzVhTMWV",
			"type": "text",
			"x": 48.92404012941642,
			"y": -175.07437054834548,
			"width": 15.29998779296875,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 956358881,
			"version": 4,
			"versionNonce": 456985135,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1718814150014,
			"link": null,
			"locked": false,
			"text": "8",
			"rawText": "8",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 18,
			"containerId": "VmuEOByQ50QaRsYj2ibwT",
			"originalText": "8",
			"lineHeight": 1.25
		},
		{
			"type": "ellipse",
			"version": 159,
			"versionNonce": 818291983,
			"isDeleted": false,
			"id": "R-6doQEL2Nyk6LZYCiTVW",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -246.65008544921875,
			"y": -78.38127899169922,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 66.4000244140625,
			"height": 65.59999084472656,
			"seed": 1099953199,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "NdlNXX6pnJ-7LDU3KwCTp",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "PbqyyFEh"
				}
			],
			"updated": 1718814150975,
			"link": null,
			"locked": false
		},
		{
			"id": "PbqyyFEh",
			"type": "text",
			"x": -220.54602212644295,
			"y": -58.274382755376735,
			"width": 14.239990234375,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1339641729,
			"version": 4,
			"versionNonce": 556770703,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1718814154216,
			"link": null,
			"locked": false,
			"text": "2",
			"rawText": "2",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 18,
			"containerId": "R-6doQEL2Nyk6LZYCiTVW",
			"originalText": "2",
			"lineHeight": 1.25
		},
		{
			"type": "ellipse",
			"version": 116,
			"versionNonce": 1519707663,
			"isDeleted": false,
			"id": "MbQOyZrq_NTdXPRIzSLz9",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -115.45001220703125,
			"y": -75.18126678466797,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 66.4000244140625,
			"height": 65.59999084472656,
			"seed": 1272653825,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "zxg9_4lfpwpA7q-Z2t7TA",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "L8pmjcF5"
				}
			],
			"updated": 1718814155049,
			"link": null,
			"locked": false
		},
		{
			"id": "L8pmjcF5",
			"type": "text",
			"x": -88.62594766355232,
			"y": -55.074370548345485,
			"width": 12.79998779296875,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1054932609,
			"version": 4,
			"versionNonce": 555083407,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1718814157564,
			"link": null,
			"locked": false,
			"text": "4",
			"rawText": "4",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 18,
			"containerId": "MbQOyZrq_NTdXPRIzSLz9",
			"originalText": "4",
			"lineHeight": 1.25
		},
		{
			"type": "ellipse",
			"version": 101,
			"versionNonce": 684893967,
			"isDeleted": false,
			"id": "QwrXGEafZSCXzOkwN6phs",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -6.04998779296875,
			"y": -76.58123016357422,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 66.4000244140625,
			"height": 65.59999084472656,
			"seed": 1153585071,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"type": "text",
					"id": "23kg61od"
				}
			],
			"updated": 1718814158453,
			"link": null,
			"locked": false
		},
		{
			"id": "23kg61od",
			"type": "text",
			"x": 21.794073393576575,
			"y": -56.474333927251735,
			"width": 10.759994506835938,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 2013900161,
			"version": 4,
			"versionNonce": 367732623,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1718814161269,
			"link": null,
			"locked": false,
			"text": "7",
			"rawText": "7",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 18,
			"containerId": "QwrXGEafZSCXzOkwN6phs",
			"originalText": "7",
			"lineHeight": 1.25
		},
		{
			"type": "ellipse",
			"version": 130,
			"versionNonce": 1197723599,
			"isDeleted": false,
			"id": "cu3SlJ_iDjIItL3dNqda-",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 93.75,
			"y": -88.98125457763672,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 66.4000244140625,
			"height": 65.59999084472656,
			"seed": 934884257,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "Fcfm6ZBVMkoq-5wmebq1D",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "6AEHeNyA"
				}
			],
			"updated": 1718814162453,
			"link": null,
			"locked": false
		},
		{
			"id": "6AEHeNyA",
			"type": "text",
			"x": 120.88406210207268,
			"y": -68.87435834131423,
			"width": 12.17999267578125,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 351525057,
			"version": 4,
			"versionNonce": 2101473359,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1718814174127,
			"link": null,
			"locked": false,
			"text": "9",
			"rawText": "9",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 18,
			"containerId": "cu3SlJ_iDjIItL3dNqda-",
			"originalText": "9",
			"lineHeight": 1.25
		},
		{
			"id": "u-UJBVuhZFL-qyIPzjJiD",
			"type": "arrow",
			"x": -61.03355951061462,
			"y": -224.76871643199001,
			"width": 38.42554488327437,
			"height": 42.3783366771616,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 430291009,
			"version": 45,
			"versionNonce": 572617039,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1718814175320,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-38.42554488327437,
					42.3783366771616
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "qb3SZ-00U1aodyWE9zShi",
				"gap": 1.536401684813633,
				"focus": -0.07808194142547877
			},
			"endBinding": {
				"elementId": "RYaD8_CkxvQXkdHmx9q1Y",
				"gap": 1.5065901303939313,
				"focus": 0.06778067144554951
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"type": "arrow",
			"version": 170,
			"versionNonce": 997653153,
			"isDeleted": false,
			"id": "NdlNXX6pnJ-7LDU3KwCTp",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -147.0039596724941,
			"y": -130.13089283780437,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 47.9589855314436,
			"height": 54.85895595944265,
			"seed": 1864776385,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1718814154268,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "RYaD8_CkxvQXkdHmx9q1Y",
				"gap": 3.318482556944957,
				"focus": -0.044294763876114224
			},
			"endBinding": {
				"elementId": "R-6doQEL2Nyk6LZYCiTVW",
				"gap": 2.0654498739185527,
				"focus": -0.170260912236739
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-47.9589855314436,
					54.85895595944265
				]
			]
		},
		{
			"type": "arrow",
			"version": 239,
			"versionNonce": 1607043489,
			"isDeleted": false,
			"id": "zxg9_4lfpwpA7q-Z2t7TA",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -105.641008080942,
			"y": -128.06555503963176,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 17.64105629548864,
			"height": 51.65888930977192,
			"seed": 1603852257,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1718814157616,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "RYaD8_CkxvQXkdHmx9q1Y",
				"gap": 2.663413939934756,
				"focus": -0.2356389696578267
			},
			"endBinding": {
				"elementId": "MbQOyZrq_NTdXPRIzSLz9",
				"gap": 1.6968998843357141,
				"focus": 0.1675131593421937
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					17.64105629548864,
					51.65888930977192
				]
			]
		},
		{
			"type": "arrow",
			"version": 309,
			"versionNonce": 1650633103,
			"isDeleted": false,
			"id": "tvu5Mfx0G-PUfZCFjXq44",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -13.035331775950691,
			"y": -225.9247223787213,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 39.58607399384414,
			"height": 44.18587954643141,
			"seed": 1714953199,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1718814175320,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "qb3SZ-00U1aodyWE9zShi",
				"gap": 4.385319180087599,
				"focus": -0.07343411520077942
			},
			"endBinding": {
				"elementId": "VmuEOByQ50QaRsYj2ibwT",
				"gap": 2.6215100842047008,
				"focus": -0.28547025000538856
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					39.58607399384414,
					44.18587954643141
				]
			]
		},
		{
			"type": "arrow",
			"version": 284,
			"versionNonce": 2062646959,
			"isDeleted": false,
			"id": "Fcfm6ZBVMkoq-5wmebq1D",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 80.65523946912343,
			"y": -136.21655902373308,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 31.241076555378342,
			"height": 42.858912470025814,
			"seed": 1875723649,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1718814174179,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "VmuEOByQ50QaRsYj2ibwT",
				"gap": 2.5940797937959843,
				"focus": -0.12301837236372343
			},
			"endBinding": {
				"elementId": "cu3SlJ_iDjIItL3dNqda-",
				"gap": 7.252944449217765,
				"focus": 0.2944107259328806
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					31.241076555378342,
					42.858912470025814
				]
			]
		},
		{
			"type": "arrow",
			"version": 214,
			"versionNonce": 1365601185,
			"isDeleted": false,
			"id": "42u8rb6R6VPbXqmfvmgsE",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 47.606536890804634,
			"y": -129.75743784425882,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 27.00633100009854,
			"height": 54.55075083087124,
			"seed": 1210375745,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1718814150066,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "VmuEOByQ50QaRsYj2ibwT",
				"gap": 1,
				"focus": -0.19501849532601262
			},
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-27.00633100009854,
					54.55075083087124
				]
			]
		},
		{
			"id": "BCirn9MW",
			"type": "text",
			"x": -176.25,
			"y": 36.01875305175781,
			"width": 318.77984619140625,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1499648463,
			"version": 50,
			"versionNonce": 655791663,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1718814193793,
			"link": null,
			"locked": false,
			"text": "5 -> 3 -> 2 -> 4 -> 8 -> 7 -> 9",
			"rawText": "5 -> 3 -> 2 -> 4 -> 8 -> 7 -> 9",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "5 -> 3 -> 2 -> 4 -> 8 -> 7 -> 9",
			"lineHeight": 1.25
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "solid",
		"currentItemStrokeWidth": 2,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 439.25,
		"scrollY": 440.8187561035156,
		"zoom": {
			"value": 1
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%