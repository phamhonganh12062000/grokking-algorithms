---
tags:
  - "#study"
  - "#review"
  - "#programming"
  - "#algorithm"
cssclasses:
  - center-images
---

We have a **leaf node** (node without children) as the node at the bottom, and there might be**single child node** while most of the nodes are **nodes with two children**. Each node can have *at most* one “left” child and one “right” child.

A node “left” descendants can only contain values that tare **LESS** than the node itself, while the “right” descendants can only contain values that are **GREATER** than the node itself

![[Pasted image 20240608155938.png|500]]

## [[Searching in binary search trees]]

## [[Insertion in binary search trees]]

## [[Deletion in binary search trees]]


## Binary search trees in action

An app that maintains a list of book titles

- Able to print the book titles in alphabetical order`
- Allow constant changes to the list
- Allow users to search for a title within a list

![[Pasted image 20240619214542.png]]


# [[Binary search tree traversal]]
