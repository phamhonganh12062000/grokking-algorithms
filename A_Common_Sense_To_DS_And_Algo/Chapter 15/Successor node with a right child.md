---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements

# Embedded files
c1eaaf2d8428e67aa7ce528a169d46174d817244: [[Pasted Image 20240612214106_774.png]]
027c335d8e79c37dcc0a918f6732986972129bb0: [[Pasted Image 20240612214121_775.png]]

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/2.0.18",
	"elements": [
		{
			"type": "image",
			"version": 61,
			"versionNonce": 1450530800,
			"isDeleted": false,
			"id": "d8ZEUGQOdHYhflQrlIFNg",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -415.3499755859375,
			"y": -206.4812774658203,
			"strokeColor": "transparent",
			"backgroundColor": "transparent",
			"width": 439,
			"height": 283,
			"seed": 120515901,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1729696132278,
			"link": null,
			"locked": false,
			"status": "pending",
			"fileId": "c1eaaf2d8428e67aa7ce528a169d46174d817244",
			"scale": [
				1,
				1
			]
		},
		{
			"type": "arrow",
			"version": 177,
			"versionNonce": 2015387408,
			"isDeleted": false,
			"id": "YYZtzD1eKcu0ynwXeOQut",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -29.45001220703125,
			"y": -79.98124694824219,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 103.20001220703122,
			"height": 6.578310028563521,
			"seed": 1727854877,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1729696132278,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": {
				"elementId": "kuHLcFukR72gNThBE47IO",
				"focus": 0.048729802303590294,
				"gap": 31.79998779296875
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					103.20001220703122,
					-6.578310028563521
				]
			]
		},
		{
			"type": "image",
			"version": 67,
			"versionNonce": 1608833520,
			"isDeleted": false,
			"id": "kuHLcFukR72gNThBE47IO",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 105.54998779296875,
			"y": -213.4812469482422,
			"strokeColor": "transparent",
			"backgroundColor": "transparent",
			"width": 370,
			"height": 239,
			"seed": 897898419,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "YYZtzD1eKcu0ynwXeOQut",
					"type": "arrow"
				}
			],
			"updated": 1729696132278,
			"link": null,
			"locked": false,
			"status": "pending",
			"fileId": "027c335d8e79c37dcc0a918f6732986972129bb0",
			"scale": [
				1,
				1
			]
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "solid",
		"currentItemStrokeWidth": 2,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 443.2333272298177,
		"scrollY": 624.8441094292534,
		"zoom": {
			"value": 0.9
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%