---
tags:
  - "#study"
  - "#review"
  - "#programming"
  - "#algorithm"
cssclasses:
  - center-images
---

> [!summary] 3-sentence summary
> 
> - Trees are a kind of doubly linked list in which we have a collection of connected nodes, and each node holds the addresses of their descendant nodes.
> - In a binary search tree, the left-side node’s value must be less than that of its parent node, while the right-side node’s value must be greater.
> 
## [[Tree-based data structure]]


## [[Binary search trees]]


# Exercises

Ex1

![[Pasted image 20240619224208.png]]

Ex2 $\log(1000)$

Ex4

![[Ex4_Traversing_PreOrder]]



Ex5

![[Ex5_Traversing_PostOrder]]