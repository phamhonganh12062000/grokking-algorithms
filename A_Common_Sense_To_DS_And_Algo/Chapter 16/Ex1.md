---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
11 ^3R3ihha3

10 ^fmAY5Hab

8 ^UwpOBeZU

6 ^uVuj6fpm

9 ^YlmKPXnS

7 ^ZNH1HIVG

4 ^ZIBezv80

2 ^WaizzKtO

1 ^SKZLENZh

3 ^vfS9QH1Q

5 ^Uo6Wi8ZB

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/2.0.18",
	"elements": [
		{
			"id": "G55G2xF1x9UgjDKkw_vEF",
			"type": "ellipse",
			"x": -5.0499267578125,
			"y": -307.9812469482422,
			"width": 59.199951171875,
			"height": 49,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1810707753,
			"version": 80,
			"versionNonce": 961668489,
			"isDeleted": false,
			"boundElements": [
				{
					"id": "mCpTJI-G5EDtnrnuh2J0e",
					"type": "arrow"
				},
				{
					"id": "lYRTdWyzxIu4rIFbsE-Va",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "3R3ihha3"
				}
			],
			"updated": 1720859983803,
			"link": null,
			"locked": false
		},
		{
			"id": "3R3ihha3",
			"type": "text",
			"x": 19.19970719940703,
			"y": -295.8053630873126,
			"width": 10.839996337890625,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 435171399,
			"version": 13,
			"versionNonce": 1429933671,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1720860091814,
			"link": null,
			"locked": false,
			"text": "11",
			"rawText": "11",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 18,
			"containerId": "G55G2xF1x9UgjDKkw_vEF",
			"originalText": "11",
			"lineHeight": 1.25
		},
		{
			"type": "ellipse",
			"version": 132,
			"versionNonce": 463179049,
			"isDeleted": false,
			"id": "1-f_DtT7Gr3HBi6XzCgu-",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -116.449951171875,
			"y": -226.3812484741211,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 59.199951171875,
			"height": 57.59999084472656,
			"seed": 1603702055,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "mCpTJI-G5EDtnrnuh2J0e",
					"type": "arrow"
				},
				{
					"id": "2-f3ZJH6eQnIcQybY-d8A",
					"type": "arrow"
				},
				{
					"id": "5sgpiNNwTZsDbPGgJiEC6",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "fmAY5Hab"
				}
			],
			"updated": 1720859989813,
			"link": null,
			"locked": false
		},
		{
			"id": "fmAY5Hab",
			"type": "text",
			"x": -96.37031538360078,
			"y": -209.94592511305243,
			"width": 19.17999267578125,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1972922055,
			"version": 8,
			"versionNonce": 1341340551,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1720860166713,
			"link": null,
			"locked": false,
			"text": "10",
			"rawText": "10",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 18,
			"containerId": "1-f_DtT7Gr3HBi6XzCgu-",
			"originalText": "10",
			"lineHeight": 1.25
		},
		{
			"type": "ellipse",
			"version": 207,
			"versionNonce": 1638385063,
			"isDeleted": false,
			"id": "kWH3F5I0glQI9sJDYz9xb",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 90.1500244140625,
			"y": -232.18123626708984,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 59.199951171875,
			"height": 57.59999084472656,
			"seed": 114611465,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "lYRTdWyzxIu4rIFbsE-Va",
					"type": "arrow"
				},
				{
					"id": "GEY0mEgR5q2RLL21aymfU",
					"type": "arrow"
				},
				{
					"id": "RGBfFWO4KddCxMAUqjYbL",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "UwpOBeZU"
				}
			],
			"updated": 1720859996115,
			"link": null,
			"locked": false
		},
		{
			"id": "UwpOBeZU",
			"type": "text",
			"x": 112.16966264374297,
			"y": -215.74591290602118,
			"width": 15.29998779296875,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1144267113,
			"version": 4,
			"versionNonce": 1607481895,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1720859997296,
			"link": null,
			"locked": false,
			"text": "8",
			"rawText": "8",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 18,
			"containerId": "kWH3F5I0glQI9sJDYz9xb",
			"originalText": "8",
			"lineHeight": 1.25
		},
		{
			"type": "ellipse",
			"version": 160,
			"versionNonce": 1187347463,
			"isDeleted": false,
			"id": "h7mSdQEm0bFhL7iPm7kAA",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -189.64996337890625,
			"y": -157.58123016357422,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 59.199951171875,
			"height": 57.59999084472656,
			"seed": 504142599,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "2-f3ZJH6eQnIcQybY-d8A",
					"type": "arrow"
				},
				{
					"id": "4xtzwjIFlQvtQSPq6CNNJ",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "uVuj6fpm"
				}
			],
			"updated": 1720860000076,
			"link": null,
			"locked": false
		},
		{
			"id": "uVuj6fpm",
			"type": "text",
			"x": -166.38032514922577,
			"y": -141.14590680250555,
			"width": 12.79998779296875,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1108800777,
			"version": 4,
			"versionNonce": 1823457415,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1720860001219,
			"link": null,
			"locked": false,
			"text": "6",
			"rawText": "6",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 18,
			"containerId": "h7mSdQEm0bFhL7iPm7kAA",
			"originalText": "6",
			"lineHeight": 1.25
		},
		{
			"type": "ellipse",
			"version": 182,
			"versionNonce": 2044047783,
			"isDeleted": false,
			"id": "VUBotMg-s9OXPMAtIz5SR",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -50.2498779296875,
			"y": -160.78124237060547,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 59.199951171875,
			"height": 57.59999084472656,
			"seed": 1546692071,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "5sgpiNNwTZsDbPGgJiEC6",
					"type": "arrow"
				},
				{
					"id": "4QYcjjRizPLJRYE3Od5t9",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "YlmKPXnS"
				},
				{
					"id": "PFhDMgfzRmA99c7yh6Yae",
					"type": "arrow"
				}
			],
			"updated": 1720860032542,
			"link": null,
			"locked": false
		},
		{
			"id": "YlmKPXnS",
			"type": "text",
			"x": -26.670242141413283,
			"y": -144.3459190095368,
			"width": 12.17999267578125,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1252880199,
			"version": 5,
			"versionNonce": 466079111,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1720860170155,
			"link": null,
			"locked": false,
			"text": "9",
			"rawText": "9",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 18,
			"containerId": "VUBotMg-s9OXPMAtIz5SR",
			"originalText": "9",
			"lineHeight": 1.25
		},
		{
			"type": "ellipse",
			"version": 150,
			"versionNonce": 1817858695,
			"isDeleted": false,
			"id": "12ZImoVA4Da3lW60erNQy",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 47.14990234375,
			"y": -159.38121795654297,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 59.199951171875,
			"height": 57.59999084472656,
			"seed": 776743497,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"type": "text",
					"id": "ZNH1HIVG"
				}
			],
			"updated": 1720860008307,
			"link": null,
			"locked": false
		},
		{
			"id": "ZNH1HIVG",
			"type": "text",
			"x": 71.43953721649687,
			"y": -142.9458945954743,
			"width": 10.759994506835938,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1190983305,
			"version": 6,
			"versionNonce": 566395207,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1720860010781,
			"link": null,
			"locked": false,
			"text": "7",
			"rawText": "7",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 18,
			"containerId": "12ZImoVA4Da3lW60erNQy",
			"originalText": "7",
			"lineHeight": 1.25
		},
		{
			"type": "ellipse",
			"version": 209,
			"versionNonce": 1281131625,
			"isDeleted": false,
			"id": "ADjbmVSJVUc4WY8PToJpE",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 171.94989013671875,
			"y": -156.7812728881836,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 59.199951171875,
			"height": 57.59999084472656,
			"seed": 475969639,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"type": "text",
					"id": "ZIBezv80"
				}
			],
			"updated": 1720860014015,
			"link": null,
			"locked": false
		},
		{
			"id": "ZIBezv80",
			"type": "text",
			"x": 195.21952836639923,
			"y": -140.34594952711493,
			"width": 12.79998779296875,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1032461191,
			"version": 6,
			"versionNonce": 1090675113,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1720860016635,
			"link": null,
			"locked": false,
			"text": "4",
			"rawText": "4",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 18,
			"containerId": "ADjbmVSJVUc4WY8PToJpE",
			"originalText": "4",
			"lineHeight": 1.25
		},
		{
			"type": "ellipse",
			"version": 205,
			"versionNonce": 1530968999,
			"isDeleted": false,
			"id": "tUyiYMTVvd-0A4Slj6wbL",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -269.6500244140625,
			"y": -75.98128509521484,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 59.199951171875,
			"height": 57.59999084472656,
			"seed": 8251751,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "4xtzwjIFlQvtQSPq6CNNJ",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "WaizzKtO"
				}
			],
			"updated": 1720860018631,
			"link": null,
			"locked": false
		},
		{
			"id": "WaizzKtO",
			"type": "text",
			"x": -247.10038740508514,
			"y": -59.54596173414617,
			"width": 14.239990234375,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 917687145,
			"version": 4,
			"versionNonce": 37629991,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1720860019630,
			"link": null,
			"locked": false,
			"text": "2",
			"rawText": "2",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 18,
			"containerId": "tUyiYMTVvd-0A4Slj6wbL",
			"originalText": "2",
			"lineHeight": 1.25
		},
		{
			"type": "ellipse",
			"version": 171,
			"versionNonce": 802544969,
			"isDeleted": false,
			"id": "Hd5nM9iviRIOXi0_tagpJ",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -152.64993286132812,
			"y": -74.38121795654297,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 59.199951171875,
			"height": 57.59999084472656,
			"seed": 1401368585,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "PzPVnb9KpzSZexbaaXacZ",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "SKZLENZh"
				}
			],
			"updated": 1720860022448,
			"link": null,
			"locked": false
		},
		{
			"id": "SKZLENZh",
			"type": "text",
			"x": -125.69029981963592,
			"y": -57.94589459547429,
			"width": 5.4199981689453125,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 513631399,
			"version": 4,
			"versionNonce": 252473545,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1720860023368,
			"link": null,
			"locked": false,
			"text": "1",
			"rawText": "1",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 18,
			"containerId": "Hd5nM9iviRIOXi0_tagpJ",
			"originalText": "1",
			"lineHeight": 1.25
		},
		{
			"type": "ellipse",
			"version": 252,
			"versionNonce": 738266535,
			"isDeleted": false,
			"id": "iWdYtS4LeOtEZlhk6ENxM",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -80.24993896484375,
			"y": -77.1812973022461,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 59.199951171875,
			"height": 57.59999084472656,
			"seed": 1383086503,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"type": "text",
					"id": "vfS9QH1Q"
				}
			],
			"updated": 1720860028412,
			"link": null,
			"locked": false
		},
		{
			"id": "vfS9QH1Q",
			"type": "text",
			"x": -57.39030439727266,
			"y": -60.74597394117742,
			"width": 13.6199951171875,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 310008169,
			"version": 4,
			"versionNonce": 793084455,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1720860030203,
			"link": null,
			"locked": false,
			"text": "3",
			"rawText": "3",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 18,
			"containerId": "iWdYtS4LeOtEZlhk6ENxM",
			"originalText": "3",
			"lineHeight": 1.25
		},
		{
			"id": "mCpTJI-G5EDtnrnuh2J0e",
			"type": "arrow",
			"x": -2.588521648421768,
			"y": -267.3885681150112,
			"width": 63.043134853948594,
			"height": 45.31645988234473,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 291945225,
			"version": 41,
			"versionNonce": 2038625511,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1720860168102,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-63.043134853948594,
					45.31645988234473
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "G55G2xF1x9UgjDKkw_vEF",
				"gap": 3.5398734331401727,
				"focus": 0.10524038064183332
			},
			"endBinding": {
				"elementId": "1-f_DtT7Gr3HBi6XzCgu-",
				"gap": 3.2682139750828867,
				"focus": -0.2580150750964515
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "2-f3ZJH6eQnIcQybY-d8A",
			"type": "arrow",
			"x": -113.04977106760872,
			"y": -177.58144500841078,
			"width": 28.000312349468985,
			"height": 24.800264445219398,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 192374185,
			"version": 33,
			"versionNonce": 2015893513,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1720860168984,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-28.000312349468985,
					24.800264445219398
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "1-f_DtT7Gr3HBi6XzCgu-",
				"gap": 3.662297828424954,
				"focus": 0.08231214768408966
			},
			"endBinding": {
				"elementId": "h7mSdQEm0bFhL7iPm7kAA",
				"gap": 1.5095153703619637,
				"focus": -0.1841396148390067
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "4xtzwjIFlQvtQSPq6CNNJ",
			"type": "arrow",
			"x": -187.44995955306007,
			"y": -115.1813076795438,
			"width": 40.800150453648826,
			"height": 37.600156006564504,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1727531529,
			"version": 41,
			"versionNonce": 635857353,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1720860168985,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-40.800150453648826,
					37.600156006564504
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "h7mSdQEm0bFhL7iPm7kAA",
				"gap": 1.152717937001782,
				"focus": 0.29371363146779184
			},
			"endBinding": {
				"elementId": "tUyiYMTVvd-0A4Slj6wbL",
				"gap": 3.708190465068558,
				"focus": -0.4922202393147026
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "lYRTdWyzxIu4rIFbsE-Va",
			"type": "arrow",
			"x": 47.99155097854817,
			"y": -263.4396000511932,
			"width": 45.76618811330919,
			"height": 43.62953314168087,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 131661033,
			"version": 48,
			"versionNonce": 820129287,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1720860093754,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					45.76618811330919,
					43.62953314168087
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "G55G2xF1x9UgjDKkw_vEF",
				"gap": 3.7096828087064786,
				"focus": -0.061694319410792064
			},
			"endBinding": {
				"elementId": "kWH3F5I0glQI9sJDYz9xb",
				"gap": 1.3838991476563969,
				"focus": -0.2070922916785622
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "GEY0mEgR5q2RLL21aymfU",
			"type": "arrow",
			"x": 145.3496673972403,
			"y": -178.38158430914194,
			"width": 34.400332602759704,
			"height": 32.00034346441538,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1996480489,
			"version": 27,
			"versionNonce": 404828423,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1720860035072,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					34.400332602759704,
					32.00034346441538
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "kWH3F5I0glQI9sJDYz9xb",
				"gap": 6.578876353631621,
				"focus": 0.029765148996180494
			},
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "RGBfFWO4KddCxMAUqjYbL",
			"type": "arrow",
			"x": 107.7501154447622,
			"y": -172.78137545633504,
			"width": 17.6000910306997,
			"height": 16.800128508092854,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1348193607,
			"version": 17,
			"versionNonce": 184776743,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1720860035072,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-17.6000910306997,
					16.800128508092854
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "kWH3F5I0glQI9sJDYz9xb",
				"gap": 3.965334723080481,
				"focus": -0.47453589566461946
			},
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "5sgpiNNwTZsDbPGgJiEC6",
			"type": "arrow",
			"x": -69.85007316252153,
			"y": -171.9813315145654,
			"width": 24.000245545307322,
			"height": 20.800200598776883,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1027702217,
			"version": 39,
			"versionNonce": 190721065,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1720860170652,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					24.000245545307322,
					20.800200598776883
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "1-f_DtT7Gr3HBi6XzCgu-",
				"gap": 1.6920552625755363,
				"focus": 0.2817499615359155
			},
			"endBinding": {
				"elementId": "VUBotMg-s9OXPMAtIz5SR",
				"gap": 2.3817287480921436,
				"focus": -0.06845058797653532
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "PzPVnb9KpzSZexbaaXacZ",
			"type": "arrow",
			"x": -148.25,
			"y": -105.58125305175781,
			"width": 15.132984829715156,
			"height": 31.85888984529086,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1987859591,
			"version": 21,
			"versionNonce": 170245767,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1720860035072,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					15.132984829715156,
					31.85888984529086
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": {
				"elementId": "Hd5nM9iviRIOXi0_tagpJ",
				"gap": 1,
				"focus": 0.10120106570442862
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "4QYcjjRizPLJRYE3Od5t9",
			"type": "arrow",
			"x": -28.28696200633486,
			"y": -103.12427608665283,
			"width": 19.16305020069639,
			"height": 29.543023034895015,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1665865225,
			"version": 29,
			"versionNonce": 351846153,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1720860170652,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-19.16305020069639,
					29.543023034895015
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "VUBotMg-s9OXPMAtIz5SR",
				"gap": 1,
				"focus": -0.3165812414412856
			},
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "PFhDMgfzRmA99c7yh6Yae",
			"type": "arrow",
			"x": -3.9211575729505768,
			"y": -102.55764884315217,
			"width": 24.12651253917073,
			"height": 29.17528441389601,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 577354121,
			"version": 40,
			"versionNonce": 1664638855,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1720860180019,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					24.12651253917073,
					29.17528441389601
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "VUBotMg-s9OXPMAtIz5SR",
				"gap": 4.856433195307076,
				"focus": 0.19794911569061463
			},
			"endBinding": {
				"elementId": "Pxt-7Tb61o0imkzfAfTf5",
				"gap": 1.7617279160170867,
				"focus": 0.14535793508814315
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"type": "ellipse",
			"version": 273,
			"versionNonce": 1611045735,
			"isDeleted": false,
			"id": "Pxt-7Tb61o0imkzfAfTf5",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 6.75,
			"y": -75.98125457763672,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 59.199951171875,
			"height": 57.59999084472656,
			"seed": 1202594313,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"type": "text",
					"id": "Uo6Wi8ZB"
				},
				{
					"id": "PFhDMgfzRmA99c7yh6Yae",
					"type": "arrow"
				}
			],
			"updated": 1720860175608,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 76,
			"versionNonce": 1050321447,
			"isDeleted": false,
			"id": "Uo6Wi8ZB",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 30.239639450383592,
			"y": -59.54593121656804,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 12.3599853515625,
			"height": 25,
			"seed": 328136937,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1720860179552,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "5",
			"rawText": "5",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "Pxt-7Tb61o0imkzfAfTf5",
			"originalText": "5",
			"lineHeight": 1.25,
			"baseline": 18
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "solid",
		"currentItemStrokeWidth": 2,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 439.25,
		"scrollY": 440.8187561035156,
		"zoom": {
			"value": 1
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%