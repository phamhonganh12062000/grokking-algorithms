---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
10 ^3R3ihha3

9 ^fmAY5Hab

8 ^UwpOBeZU

6 ^uVuj6fpm

5 ^YlmKPXnS

7 ^ZNH1HIVG

4 ^ZIBezv80

2 ^WaizzKtO

1 ^SKZLENZh

3 ^vfS9QH1Q

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/2.0.18",
	"elements": [
		{
			"type": "ellipse",
			"version": 81,
			"versionNonce": 286515975,
			"isDeleted": false,
			"id": "G55G2xF1x9UgjDKkw_vEF",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -4.378709269308786,
			"y": -307.9812469482422,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 59.199951171875,
			"height": 49,
			"seed": 1810707753,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "mCpTJI-G5EDtnrnuh2J0e",
					"type": "arrow"
				},
				{
					"id": "lYRTdWyzxIu4rIFbsE-Va",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "3R3ihha3"
				}
			],
			"updated": 1720861209347,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 16,
			"versionNonce": 861060073,
			"isDeleted": false,
			"id": "3R3ihha3",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 15.700926518965431,
			"y": -295.8053630873126,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 19.17999267578125,
			"height": 25,
			"seed": 435171399,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1720861210952,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "10",
			"rawText": "10",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "G55G2xF1x9UgjDKkw_vEF",
			"originalText": "10",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "ellipse",
			"version": 132,
			"versionNonce": 463179049,
			"isDeleted": false,
			"id": "1-f_DtT7Gr3HBi6XzCgu-",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -116.449951171875,
			"y": -226.3812484741211,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 59.199951171875,
			"height": 57.59999084472656,
			"seed": 1603702055,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "mCpTJI-G5EDtnrnuh2J0e",
					"type": "arrow"
				},
				{
					"id": "2-f3ZJH6eQnIcQybY-d8A",
					"type": "arrow"
				},
				{
					"id": "5sgpiNNwTZsDbPGgJiEC6",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "fmAY5Hab"
				}
			],
			"updated": 1720859989813,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 9,
			"versionNonce": 1927902471,
			"isDeleted": false,
			"id": "fmAY5Hab",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -92.87031538360078,
			"y": -209.94592511305243,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 12.17999267578125,
			"height": 25,
			"seed": 1972922055,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1720861221907,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "9",
			"rawText": "9",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "1-f_DtT7Gr3HBi6XzCgu-",
			"originalText": "9",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "ellipse",
			"version": 207,
			"versionNonce": 1638385063,
			"isDeleted": false,
			"id": "kWH3F5I0glQI9sJDYz9xb",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 90.1500244140625,
			"y": -232.18123626708984,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 59.199951171875,
			"height": 57.59999084472656,
			"seed": 114611465,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "lYRTdWyzxIu4rIFbsE-Va",
					"type": "arrow"
				},
				{
					"id": "GEY0mEgR5q2RLL21aymfU",
					"type": "arrow"
				},
				{
					"id": "RGBfFWO4KddCxMAUqjYbL",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "UwpOBeZU"
				}
			],
			"updated": 1720859996115,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 4,
			"versionNonce": 1607481895,
			"isDeleted": false,
			"id": "UwpOBeZU",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 112.16966264374297,
			"y": -215.74591290602118,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 15.29998779296875,
			"height": 25,
			"seed": 1144267113,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1720859997296,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "8",
			"rawText": "8",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "kWH3F5I0glQI9sJDYz9xb",
			"originalText": "8",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "ellipse",
			"version": 160,
			"versionNonce": 1187347463,
			"isDeleted": false,
			"id": "h7mSdQEm0bFhL7iPm7kAA",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -189.64996337890625,
			"y": -157.58123016357422,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 59.199951171875,
			"height": 57.59999084472656,
			"seed": 504142599,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "2-f3ZJH6eQnIcQybY-d8A",
					"type": "arrow"
				},
				{
					"id": "4xtzwjIFlQvtQSPq6CNNJ",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "uVuj6fpm"
				}
			],
			"updated": 1720860000076,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 4,
			"versionNonce": 1823457415,
			"isDeleted": false,
			"id": "uVuj6fpm",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -166.38032514922577,
			"y": -141.14590680250555,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 12.79998779296875,
			"height": 25,
			"seed": 1108800777,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1720860001219,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "6",
			"rawText": "6",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "h7mSdQEm0bFhL7iPm7kAA",
			"originalText": "6",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "ellipse",
			"version": 183,
			"versionNonce": 136647847,
			"isDeleted": false,
			"id": "VUBotMg-s9OXPMAtIz5SR",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -50.2498779296875,
			"y": -160.78124237060547,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 59.199951171875,
			"height": 57.59999084472656,
			"seed": 1546692071,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "5sgpiNNwTZsDbPGgJiEC6",
					"type": "arrow"
				},
				{
					"id": "4QYcjjRizPLJRYE3Od5t9",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "YlmKPXnS"
				}
			],
			"updated": 1720861233076,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 6,
			"versionNonce": 114959561,
			"isDeleted": false,
			"id": "YlmKPXnS",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -26.760238479303908,
			"y": -144.3459190095368,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 12.3599853515625,
			"height": 25,
			"seed": 1252880199,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1720861225807,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "5",
			"rawText": "5",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "VUBotMg-s9OXPMAtIz5SR",
			"originalText": "5",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "ellipse",
			"version": 151,
			"versionNonce": 1040322793,
			"isDeleted": false,
			"id": "12ZImoVA4Da3lW60erNQy",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 47.821119832253714,
			"y": -159.38121795654297,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 59.199951171875,
			"height": 57.59999084472656,
			"seed": 776743497,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"type": "text",
					"id": "ZNH1HIVG"
				}
			],
			"updated": 1720861228801,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 7,
			"versionNonce": 994184137,
			"isDeleted": false,
			"id": "ZNH1HIVG",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 72.11075470500059,
			"y": -142.9458945954743,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 10.759994506835938,
			"height": 25,
			"seed": 1190983305,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1720861228801,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "7",
			"rawText": "7",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "12ZImoVA4Da3lW60erNQy",
			"originalText": "7",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "ellipse",
			"version": 209,
			"versionNonce": 1281131625,
			"isDeleted": false,
			"id": "ADjbmVSJVUc4WY8PToJpE",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 171.94989013671875,
			"y": -156.7812728881836,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 59.199951171875,
			"height": 57.59999084472656,
			"seed": 475969639,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"type": "text",
					"id": "ZIBezv80"
				}
			],
			"updated": 1720860014015,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 6,
			"versionNonce": 1090675113,
			"isDeleted": false,
			"id": "ZIBezv80",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 195.21952836639923,
			"y": -140.34594952711493,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 12.79998779296875,
			"height": 25,
			"seed": 1032461191,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1720860016635,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "4",
			"rawText": "4",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "ADjbmVSJVUc4WY8PToJpE",
			"originalText": "4",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "ellipse",
			"version": 205,
			"versionNonce": 1530968999,
			"isDeleted": false,
			"id": "tUyiYMTVvd-0A4Slj6wbL",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -269.6500244140625,
			"y": -75.98128509521484,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 59.199951171875,
			"height": 57.59999084472656,
			"seed": 8251751,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "4xtzwjIFlQvtQSPq6CNNJ",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "WaizzKtO"
				}
			],
			"updated": 1720860018631,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 4,
			"versionNonce": 37629991,
			"isDeleted": false,
			"id": "WaizzKtO",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -247.10038740508514,
			"y": -59.54596173414617,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 14.239990234375,
			"height": 25,
			"seed": 917687145,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1720860019630,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "2",
			"rawText": "2",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "tUyiYMTVvd-0A4Slj6wbL",
			"originalText": "2",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "ellipse",
			"version": 171,
			"versionNonce": 802544969,
			"isDeleted": false,
			"id": "Hd5nM9iviRIOXi0_tagpJ",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -152.64993286132812,
			"y": -74.38121795654297,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 59.199951171875,
			"height": 57.59999084472656,
			"seed": 1401368585,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "PzPVnb9KpzSZexbaaXacZ",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "SKZLENZh"
				}
			],
			"updated": 1720860022448,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 4,
			"versionNonce": 252473545,
			"isDeleted": false,
			"id": "SKZLENZh",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -125.69029981963592,
			"y": -57.94589459547429,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 5.4199981689453125,
			"height": 25,
			"seed": 513631399,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1720860023368,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "1",
			"rawText": "1",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "Hd5nM9iviRIOXi0_tagpJ",
			"originalText": "1",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "ellipse",
			"version": 252,
			"versionNonce": 738266535,
			"isDeleted": false,
			"id": "iWdYtS4LeOtEZlhk6ENxM",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -80.24993896484375,
			"y": -77.1812973022461,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 59.199951171875,
			"height": 57.59999084472656,
			"seed": 1383086503,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"type": "text",
					"id": "vfS9QH1Q"
				}
			],
			"updated": 1720860028412,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 4,
			"versionNonce": 793084455,
			"isDeleted": false,
			"id": "vfS9QH1Q",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -57.39030439727266,
			"y": -60.74597394117742,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 13.6199951171875,
			"height": 25,
			"seed": 310008169,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1720860030203,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "3",
			"rawText": "3",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "iWdYtS4LeOtEZlhk6ENxM",
			"originalText": "3",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "arrow",
			"version": 51,
			"versionNonce": 1600233063,
			"isDeleted": false,
			"id": "mCpTJI-G5EDtnrnuh2J0e",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -1.9723064550692122,
			"y": -267.4551766793876,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 63.589050360658334,
			"height": 45.44121084509061,
			"seed": 291945225,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1720861223940,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "G55G2xF1x9UgjDKkw_vEF",
				"gap": 3.5398734331401727,
				"focus": 0.10524038064183332
			},
			"endBinding": {
				"elementId": "1-f_DtT7Gr3HBi6XzCgu-",
				"gap": 3.2682139750828867,
				"focus": -0.2580150750964515
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-63.589050360658334,
					45.44121084509061
				]
			]
		},
		{
			"type": "arrow",
			"version": 39,
			"versionNonce": 1469870247,
			"isDeleted": false,
			"id": "2-f3ZJH6eQnIcQybY-d8A",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -113.04977106760872,
			"y": -177.58144500841078,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 28.000312349468985,
			"height": 24.800264445219398,
			"seed": 192374185,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1720861223940,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "1-f_DtT7Gr3HBi6XzCgu-",
				"gap": 3.662297828424954,
				"focus": 0.08231214768408966
			},
			"endBinding": {
				"elementId": "h7mSdQEm0bFhL7iPm7kAA",
				"gap": 1.5095153703619637,
				"focus": -0.1841396148390067
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-28.000312349468985,
					24.800264445219398
				]
			]
		},
		{
			"type": "arrow",
			"version": 45,
			"versionNonce": 1733419879,
			"isDeleted": false,
			"id": "4xtzwjIFlQvtQSPq6CNNJ",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -187.44995955306007,
			"y": -115.1813076795438,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 40.800150453648826,
			"height": 37.600156006564504,
			"seed": 1727531529,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1720861206121,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "h7mSdQEm0bFhL7iPm7kAA",
				"gap": 1.152717937001782,
				"focus": 0.29371363146779184
			},
			"endBinding": {
				"elementId": "tUyiYMTVvd-0A4Slj6wbL",
				"gap": 3.708190465068558,
				"focus": -0.4922202393147026
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-40.800150453648826,
					37.600156006564504
				]
			]
		},
		{
			"type": "arrow",
			"version": 56,
			"versionNonce": 420202407,
			"isDeleted": false,
			"id": "lYRTdWyzxIu4rIFbsE-Va",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 48.57133538649896,
			"y": -263.3631924519558,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 45.249316075433455,
			"height": 43.459048192951855,
			"seed": 131661033,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1720861213755,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "G55G2xF1x9UgjDKkw_vEF",
				"gap": 3.7096828087064786,
				"focus": -0.061694319410792064
			},
			"endBinding": {
				"elementId": "kWH3F5I0glQI9sJDYz9xb",
				"gap": 1.3838991476563969,
				"focus": -0.2070922916785622
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					45.249316075433455,
					43.459048192951855
				]
			]
		},
		{
			"type": "arrow",
			"version": 28,
			"versionNonce": 1461298663,
			"isDeleted": false,
			"id": "GEY0mEgR5q2RLL21aymfU",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 145.3496673972403,
			"y": -178.38158430914194,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 34.400332602759704,
			"height": 32.00034346441538,
			"seed": 1996480489,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1720861206120,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "kWH3F5I0glQI9sJDYz9xb",
				"gap": 6.578876353631621,
				"focus": 0.029765148996180494
			},
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					34.400332602759704,
					32.00034346441538
				]
			]
		},
		{
			"type": "arrow",
			"version": 18,
			"versionNonce": 1088049415,
			"isDeleted": false,
			"id": "RGBfFWO4KddCxMAUqjYbL",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 107.7501154447622,
			"y": -172.78137545633504,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 17.6000910306997,
			"height": 16.800128508092854,
			"seed": 1348193607,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1720861206120,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "kWH3F5I0glQI9sJDYz9xb",
				"gap": 3.965334723080481,
				"focus": -0.47453589566461946
			},
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-17.6000910306997,
					16.800128508092854
				]
			]
		},
		{
			"type": "arrow",
			"version": 47,
			"versionNonce": 254280327,
			"isDeleted": false,
			"id": "5sgpiNNwTZsDbPGgJiEC6",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -69.85007316252153,
			"y": -171.9813315145654,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 24.000245545307322,
			"height": 20.800200598776883,
			"seed": 1027702217,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1720861226694,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "1-f_DtT7Gr3HBi6XzCgu-",
				"gap": 1.6920552625755363,
				"focus": 0.2817499615359155
			},
			"endBinding": {
				"elementId": "VUBotMg-s9OXPMAtIz5SR",
				"gap": 2.3817287480921436,
				"focus": -0.06845058797653532
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					24.000245545307322,
					20.800200598776883
				]
			]
		},
		{
			"type": "arrow",
			"version": 22,
			"versionNonce": 1781456519,
			"isDeleted": false,
			"id": "PzPVnb9KpzSZexbaaXacZ",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -148.25,
			"y": -105.58125305175781,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 15.132984829715156,
			"height": 31.85888984529086,
			"seed": 1987859591,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1720861206121,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": {
				"elementId": "Hd5nM9iviRIOXi0_tagpJ",
				"gap": 1,
				"focus": 0.10120106570442862
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					15.132984829715156,
					31.85888984529086
				]
			]
		},
		{
			"type": "arrow",
			"version": 31,
			"versionNonce": 1815353767,
			"isDeleted": false,
			"id": "4QYcjjRizPLJRYE3Od5t9",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -28.28696200633486,
			"y": -103.12427608665283,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 19.16305020069639,
			"height": 29.543023034895015,
			"seed": 1665865225,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1720861226694,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "VUBotMg-s9OXPMAtIz5SR",
				"gap": 1,
				"focus": -0.3165812414412856
			},
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-19.16305020069639,
					29.543023034895015
				]
			]
		},
		{
			"type": "arrow",
			"version": 47,
			"versionNonce": 1279746665,
			"isDeleted": true,
			"id": "PFhDMgfzRmA99c7yh6Yae",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -3.883204277399294,
			"y": -102.5782673717884,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 24.10513047762373,
			"height": 29.186213110896915,
			"seed": 577354121,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1720861233075,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "VUBotMg-s9OXPMAtIz5SR",
				"gap": 4.856433195307076,
				"focus": 0.19794911569061463
			},
			"endBinding": {
				"elementId": "Pxt-7Tb61o0imkzfAfTf5",
				"gap": 1.7617279160170867,
				"focus": 0.14535793508814315
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					24.10513047762373,
					29.186213110896915
				]
			]
		},
		{
			"type": "ellipse",
			"version": 275,
			"versionNonce": 1292493319,
			"isDeleted": true,
			"id": "Pxt-7Tb61o0imkzfAfTf5",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 6.75,
			"y": -75.98125457763672,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 59.199951171875,
			"height": 57.59999084472656,
			"seed": 1202594313,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"type": "text",
					"id": "Uo6Wi8ZB"
				}
			],
			"updated": 1720861233840,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 77,
			"versionNonce": 1250059753,
			"isDeleted": true,
			"id": "Uo6Wi8ZB",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 30.239639450383592,
			"y": -59.54593121656804,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 12.3599853515625,
			"height": 25,
			"seed": 328136937,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1720861233840,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "5",
			"rawText": "5",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "Pxt-7Tb61o0imkzfAfTf5",
			"originalText": "5",
			"lineHeight": 1.25,
			"baseline": 18
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "solid",
		"currentItemStrokeWidth": 2,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 356.98524833200577,
		"scrollY": 419.64194957861787,
		"zoom": {
			"value": 1.1918458721212473
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%