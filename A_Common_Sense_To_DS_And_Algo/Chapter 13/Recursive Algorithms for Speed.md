---
tags:
  - "#study"
  - "#review"
  - "#algorithm"
  - "#programming"
cssclasses:
  - center-images
---

> [!summary] 3-sentence summary
> 
> - Partitioning is when we select an element in the list as the pivot, so that all elements less than the pivot come before it and all elements greater than the pivot come after it.
> - Quicksort requires us to partition the array until the pivot is in its correct position, then we recursively partition each sub-array until we have a sub array with zero or one element.
> - Quickselect is a combination of quicksort and binary search, and it is useful when we want to retrieve the second-lowest value.


> [!info]
> 
> In many of programming languages, the sorting algorithm that is employed under the hood is Quicksort.


[[Partitioning]]

[[Quicksort]]

[[Quickselect]]


