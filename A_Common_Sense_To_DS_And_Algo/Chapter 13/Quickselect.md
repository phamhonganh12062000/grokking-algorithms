---
tags:
  - "#study"
  - "#review"
  - "#algorithm"
  - "#programming"
cssclasses:
  - center-images
---

> [!tldr]
> Quickselect = Quicksort + Binary search
>

[[How does quickselect work]]

### The efficiency of Quickselect

- Quickselect takes $O(N)$ for average scenarios