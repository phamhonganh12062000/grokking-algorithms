---
tags:
  - "#study"
  - "#review"
  - "#algorithm"
  - "#programming"
cssclasses:
  - center-images
---
Quicksort has primary types of steps:
- Comparisons: Compare each each value at hand to the pivot
- Swaps: Swap the value being pointed to by the left and right pointers

A partition runs in $O(N)$ time, while the efficiency of quicksort is $O(N\log N)$ as we break the array + its sub arrays to halves to size 1

![[Pasted image 20240505170822.png]]