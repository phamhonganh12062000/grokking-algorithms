---
tags:
  - "#study"
  - "#review"
  - "#programming"
  - "#algorithm"
cssclasses:
  - center-images
---

> [!summary] 3-sentence summary
> 
> - Overlapping sub-problems occur when the same sub-problems are solved multiple times during the computation of the overall problem.
> - We can store the results of sub-problems by using memoization (top-down) or tabulation (bottom-up) to avoid re-computations.

> [!info] What is Dynamic Programming?
> 
> Dynamic programming is the process of **optimizing recursive problems** that have overlapping subproblems.

[[Unnecessary recursive calls]]

> [!important]
> 
> Avoiding extra recursive calls is key to keeping recursion fast. 


[[Overlapping Subproblems]]

[[Approaches in Dynamic Programming]]





