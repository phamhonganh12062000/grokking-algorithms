---
tags:
  - "#study"
  - "#review"
  - "#programming"
  - "#algorithm"
cssclasses:
  - center-images
---
Which one is better? => It depends on the problems and why you are using recursion in the 1st place

> [!warning]
> 
> Even with memoization, recursion DOES carry some extra overhead versus iteration.