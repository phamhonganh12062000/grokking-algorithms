---
tags:
  - "#study"
  - "#review"
  - "#programming"
  - "#algorithm"
cssclasses:
  - center-images
---

> [!tldr]
> 
> Going Bottom-Up means we **ditch recursion** and **use other approaches** to solve the same problem.

- Going bottom-up becomes more of a “technique” when the problem is more naturally solved with recursion.