---
tags:
  - "#study"
  - "#review"
cssclasses:
  - center-images
---

## Dealing with multiple datasets

### Scenario

- An algorithm that accepts **two** arrays of numbers and returns the product of every combination of two numbers
- Two arrays do not share the same number of elements

> [!important]
> 
> Whenever we have two distinct datasets that have to interact with each other through multiplication, we have to **identify both sources separately** when we describe the efficiency in terms of Big O.

- Time complexity: $O(N*M)$



---

## Exercises

1. $O(N)$
2. $O(N + M)$ or $O(N)$ with $N$ as the total number of elements in both arrays
3. $O(N * M)$
4. $O(N^3)$
5. $O(\log N)$

