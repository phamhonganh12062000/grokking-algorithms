---
tags:
  - "#study"
  - "#review"
  - "#algorithm"
  - "#programming"
cssclasses:
  - center-images
---
- Instead of being a contiguous block of memory (array), the data from linked lists can be **scattered across different cells** throughout the computer’s memory. => Connected data that is dispersed throughout memory are known as **nodes**.
- In a linked list, each node represents one item in the list.
- Each node also comes with a little extra information, namely, **the memory address of the next node** in the list (link).

![[Pasted image 20240516212442.png]]


- A linked list’s first node can also be referred to as its **head**, and its final node as its **tail**.

> [!important]
> 
> Linked lists are an amazing data structure for moving through an entire list while making insertions or deletions, as **we never have to worry about shifting other data** as we make an insertion or deletion.


[[Reading & Searching in linked list]]

[[Insertion in linked list]]

[[Deletion in linked list]]

[[Efficiency of linked-list operations]]



