---
tags:
  - "#study"
  - "#review"
  - "#algorithm"
  - "#programming"
cssclasses:
  - center-images
---
In contrast with an array, the linked list provides **the flexibility of inserting data to the front of the list without requiring the shifting of any data**.


> [!warning]
> 
> To insert an element into the Nth index of a linked list, the linked list will have to access the Nth element and then insert the element => $O(N)$

