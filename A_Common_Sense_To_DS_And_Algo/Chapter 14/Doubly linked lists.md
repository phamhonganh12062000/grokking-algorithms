---
tags:
  - "#study"
  - "#review"
  - "#programming"
  - "#algorithm"
cssclasses:
  - center-images
---
- A doubly linked list is like a linked list except that **each node has two links**—one that points to the next node, and another that points to the previous node
- The doubly linked list always keeps track of both the first and last nodes, instead of just the first node. =>  We can access each of them in a single step, or $O(1)$ => Perfect data structure for a queue

![[Pasted image 20240517230614.png]]