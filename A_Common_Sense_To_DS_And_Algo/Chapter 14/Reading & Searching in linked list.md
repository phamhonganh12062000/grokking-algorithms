---
tags:
  - "#study"
  - "#review"
  - "#algorithm"
  - "#programming"
cssclasses:
  - center-images
---
To read the $Nth$ node in the list, the computer takes $N$ steps as we must **start at the head of the list** and **traverse each node until we reach the desired node**

Linked lists having a worst-case read of $O(N)$ is a *major disadvantage* when compared with array