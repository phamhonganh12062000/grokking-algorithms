---
tags:
  - "#study"
  - "#review"
  - "#algorithm"
  - "#programming"
cssclasses:
  - center-images
---
- Delete the 1st node: Change the `first_node` to point to the second node
- Delete the final node: Make the link of the second-to-last node null
