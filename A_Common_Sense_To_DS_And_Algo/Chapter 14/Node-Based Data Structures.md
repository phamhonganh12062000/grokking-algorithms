
> [!summary] 3-sentence summary
> 
> - A linked list contains a series of nodes with each one stores a value and the memory address of the next/previous node in the list.
> - While linked lists have a slow reading/searching speed of $O(\log(N))$, they could be either super fast $O(1)$ or equally slow $O(N)$ when it comes to insertion.
> - A doubly linked list has each node holding the memory addresses of both the previous and the next node, which guarantees $O(1)$ access time.

[[Linked Lists]]

## [[Doubly linked lists]]






