---
tags:
  - "#study"
  - "#review"
  - "#algorithm"
  - "#programming"
cssclasses:
  - center-images
---

| Operation | Array                        | Linked list                        |
| --------- | ---------------------------- | ---------------------------------- |
| Reading   | $O(1)$                       | $O(N)$                             |
| Search    | $O(N)$                       | $O(N)$                             |
| Insertion | $O(N)$ and $O(1)$ at the end | $O(N)$ and $O(1)$ at the beginning |
| Deletion  | $O(N)$ and $O(1)$ at the end | $O(N)$ and $O(1)$ at the beginning |

| Scneario                | Array        | Linked List  |
| ----------------------- | ------------ | ------------ |
| Insert at the beginning | Worst case   | Best case    |
| Insert at the middle    | Average case | Average case |
| Insert at the end       | Best case    | Worst case   |
| Delete at the beginning | Worst case   | Best case    |
| Delete at the middle    | Average case | Average case |
| Delete at the end       | Best case    | Worst case   |
