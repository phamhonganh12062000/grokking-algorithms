## How to read this book

- *Do not skip chapters*, as each one assumes you have read the previous ones
- Do not take a particular term as the textbook definition until you have completed the entire section on that topic

## Code examples

- Language-agnostic: Examples are written in Python/Ruby/JS
- Code snippets have room for improvement
