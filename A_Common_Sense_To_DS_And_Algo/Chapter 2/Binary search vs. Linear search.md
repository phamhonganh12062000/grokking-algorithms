---
tags:
  - "#study"
  - "#review"
cssclasses:
  - center-images
---
- For binary search, each time we double the data, the binary search algorithm adds **just one more step**, while there are **as many steps as there are items** for linear search

![[Pasted image 20240114134051.png]]

> [!tip] Trade-off
> 
> By using an ordered array, you have somewhat slower insertion, but much faster search.

