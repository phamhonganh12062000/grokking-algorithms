---
tags:
  - "#study"
  - "#review"
cssclasses:
  - center-images
---
- When *inserting* an element into an ordered array, we need to **always conduct a search before the actual insertion** to **determine the correct spot for the insertion.**
- The number of steps for insertion **remains similar** no matter where in the ordered array our new value ends up.
- Steps to take: $O(n+1)$

[[Searching in an ordered array]]

