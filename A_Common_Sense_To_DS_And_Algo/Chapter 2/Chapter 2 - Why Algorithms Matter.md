---
tags:
  - "#study"
  - "#review"
cssclasses:
  - center-images
---

> [!summary] 3-sentence summary
> 
> - When searching through an ordered array, we can either do the linear search $O(N)$ or the binary search $O(\log(N))$.
> - Linear search requires checking each cell one at a time from left to right.
> - Binary search requires dividing the array in half and searching the value inside the half we assume to contain it.

[[Ordered Arrays]]

[[Binary search]]

[[Binary search vs. Linear search]]

[[Why Algorithms Matter - Exercises]]

