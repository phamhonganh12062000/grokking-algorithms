---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
Base ^6JlPOPiF

LIFO ^Fl9cJek7

abcd ^BCFIIys7

bcd ^sbVwvwhZ

cd ^LHSVVbWE

d ^LlU2SMSW

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/2.0.18",
	"elements": [
		{
			"id": "c4xr2LnkvWwzQEuj9L6SE",
			"type": "rectangle",
			"x": -180.6500244140625,
			"y": -135.13750457763672,
			"width": 237.5999755859375,
			"height": 37.59999084472656,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"seed": 644930493,
			"version": 88,
			"versionNonce": 204270067,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713096174148,
			"link": null,
			"locked": false
		},
		{
			"type": "rectangle",
			"version": 38,
			"versionNonce": 319276157,
			"isDeleted": false,
			"id": "tthukWG3yEmZr13zMba2z",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -185.04998779296875,
			"y": -261.93751525878906,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 237.5999755859375,
			"height": 37.59999084472656,
			"seed": 481047603,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"id": "Ral1pS5IDkzi5Hsat2S08",
					"type": "arrow"
				},
				{
					"id": "Bvvolp8OGDKE-Jp5Prv_f",
					"type": "arrow"
				}
			],
			"updated": 1713096239035,
			"link": null,
			"locked": false
		},
		{
			"type": "rectangle",
			"version": 86,
			"versionNonce": 1450629363,
			"isDeleted": false,
			"id": "kC9BBj3eYjpy2ZbvlAckX",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -183.449951171875,
			"y": -178.7375030517578,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 237.5999755859375,
			"height": 37.59999084472656,
			"seed": 731734333,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [],
			"updated": 1713096171865,
			"link": null,
			"locked": false
		},
		{
			"type": "rectangle",
			"version": 98,
			"versionNonce": 1580303645,
			"isDeleted": false,
			"id": "kIDbX7UCKwomDnfF_JWQy",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -185.04998779296875,
			"y": -218.13751220703125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 237.5999755859375,
			"height": 37.59999084472656,
			"seed": 1466315859,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [],
			"updated": 1713096170675,
			"link": null,
			"locked": false
		},
		{
			"id": "Ral1pS5IDkzi5Hsat2S08",
			"type": "arrow",
			"x": 59.35003662109375,
			"y": -240.73751068115234,
			"width": 73.5999755859375,
			"height": 3.20001220703125,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 2097922845,
			"version": 26,
			"versionNonce": 524456349,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713096185384,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					73.5999755859375,
					-3.20001220703125
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "tthukWG3yEmZr13zMba2z",
				"focus": 0.3280127811028763,
				"gap": 6.800048828125
			},
			"endBinding": {
				"elementId": "6JlPOPiF",
				"focus": 0.2239221992708069,
				"gap": 15.20001220703125
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "6JlPOPiF",
			"type": "text",
			"x": 148.1500244140625,
			"y": -255.13753509521484,
			"width": 49.679962158203125,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1321722323,
			"version": 33,
			"versionNonce": 1201165821,
			"isDeleted": false,
			"boundElements": [
				{
					"id": "Ral1pS5IDkzi5Hsat2S08",
					"type": "arrow"
				}
			],
			"updated": 1713096185384,
			"link": null,
			"locked": false,
			"text": "Base",
			"rawText": "Base",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "Base",
			"lineHeight": 1.25
		},
		{
			"id": "Fl9cJek7",
			"type": "text",
			"x": -87.8499755859375,
			"y": -71.93753814697266,
			"width": 48.879974365234375,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 2104921693,
			"version": 28,
			"versionNonce": 339223037,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713096198720,
			"link": null,
			"locked": false,
			"text": "LIFO",
			"rawText": "LIFO",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "LIFO",
			"lineHeight": 1.25
		},
		{
			"id": "Bvvolp8OGDKE-Jp5Prv_f",
			"type": "arrow",
			"x": -68.64996337890625,
			"y": -339.9375228881836,
			"width": 1.5999755859375,
			"height": 71.20001220703125,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1448756723,
			"version": 28,
			"versionNonce": 368937843,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713096210518,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					1.5999755859375,
					71.20001220703125
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": {
				"elementId": "tthukWG3yEmZr13zMba2z",
				"focus": -0.0018848461068894798,
				"gap": 6.799995422363281
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "BCFIIys7",
			"type": "text",
			"x": -95.04998779296875,
			"y": -125.53748321533203,
			"width": 44.91996765136719,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 689015475,
			"version": 19,
			"versionNonce": 496547293,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713096216780,
			"link": null,
			"locked": false,
			"text": "abcd",
			"rawText": "abcd",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "abcd",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 55,
			"versionNonce": 1573155507,
			"isDeleted": false,
			"id": "sbVwvwhZ",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -87.7099838256836,
			"y": -169.2375259399414,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 31.579971313476562,
			"height": 25,
			"seed": 1262527133,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1713096223395,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "bcd",
			"rawText": "bcd",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "bcd",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 87,
			"versionNonce": 511917683,
			"isDeleted": false,
			"id": "LHSVVbWE",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -89.83997344970703,
			"y": -212.8375015258789,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 21.41998291015625,
			"height": 25,
			"seed": 1288122653,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1713096236493,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "cd",
			"rawText": "cd",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "cd",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 108,
			"versionNonce": 574353843,
			"isDeleted": false,
			"id": "LlU2SMSW",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -83.95999145507812,
			"y": -255.43753814697266,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 11.379989624023438,
			"height": 25,
			"seed": 1047157149,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1713096244196,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "d",
			"rawText": "d",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "d",
			"lineHeight": 1.25,
			"baseline": 18
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "solid",
		"currentItemStrokeWidth": 2,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 469.25,
		"scrollY": 416.8625183105469,
		"zoom": {
			"value": 1
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%