---
tags:
  - "#study"
  - "#review"
cssclasses:
  - center-images
---
### Steps

1. Identify the sub-problems to solve the overall problem
2. Define the recurrence relation: Solutions from sub-problems can accumulate to the solution to the overall problem
3. Decide the order of solving sub-problems
4. Compute the solution from the value of the smallest sub-problem (base case)
5. Extract the solution

[[Fibonacci sequence]]

