---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
d ^rL5FiIZ3

c ^DwtjveuB

["cd", "dc"] ^iQFxv9ga

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/2.0.18",
	"elements": [
		{
			"id": "rL5FiIZ3",
			"type": "text",
			"x": -55.8499755859375,
			"y": -136.7375259399414,
			"width": 11.379989624023438,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1491455859,
			"version": 4,
			"versionNonce": 376801245,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713103929772,
			"link": null,
			"locked": false,
			"text": "d",
			"rawText": "d",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "d",
			"lineHeight": 1.25
		},
		{
			"id": "P-GQObPJshz2iYgzsxzSW",
			"type": "arrow",
			"x": -59.04998779296875,
			"y": -178.33753204345703,
			"width": 36,
			"height": 48,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 28475475,
			"version": 48,
			"versionNonce": 1533882483,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713103929772,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-36,
					48
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "DwtjveuB",
				"focus": 0.15248457558137377,
				"gap": 4
			},
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "LQxOOF77GVISURmXJW3hx",
			"type": "arrow",
			"x": -35.8499755859375,
			"y": -176.7375259399414,
			"width": 28,
			"height": 37.600006103515625,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1906402739,
			"version": 72,
			"versionNonce": 1100121149,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713103929772,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					28,
					37.600006103515625
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "DwtjveuB",
				"focus": -0.43353755695816065,
				"gap": 9.160018920898438
			},
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "DwtjveuB",
			"type": "text",
			"x": -55.04998779296875,
			"y": -199.93750762939453,
			"width": 10.039993286132812,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 2099431901,
			"version": 12,
			"versionNonce": 972335635,
			"isDeleted": false,
			"boundElements": [
				{
					"id": "P-GQObPJshz2iYgzsxzSW",
					"type": "arrow"
				},
				{
					"id": "LQxOOF77GVISURmXJW3hx",
					"type": "arrow"
				}
			],
			"updated": 1713103929772,
			"link": null,
			"locked": false,
			"text": "c",
			"rawText": "c",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "c",
			"lineHeight": 1.25
		},
		{
			"id": "iQFxv9ga",
			"type": "text",
			"x": -101.45001220703125,
			"y": -90.33753204345703,
			"width": 104.03994750976562,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 428441939,
			"version": 27,
			"versionNonce": 1514665683,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713103948422,
			"link": null,
			"locked": false,
			"text": "[\"cd\", \"dc\"]",
			"rawText": "[\"cd\", \"dc\"]",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "[\"cd\", \"dc\"]",
			"lineHeight": 1.25
		},
		{
			"id": "mG512VYv6F21QhSIoSNWP",
			"type": "arrow",
			"x": 35.35003662109375,
			"y": -171.13751983642578,
			"width": 56,
			"height": 3.199981689453125,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1930343293,
			"version": 27,
			"versionNonce": 210826397,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1713103929772,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					56,
					-3.199981689453125
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": "arrow"
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "solid",
		"currentItemStrokeWidth": 2,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 469.25,
		"scrollY": 416.8625183105469,
		"zoom": {
			"value": 1
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%