---
tags:
  - "#study"
  - "#review"
cssclasses:
  - center-images
sr-due: 2024-10-17
sr-interval: 3
sr-ease: 250
---
- Scenario: Write a function that **returns an array of all anagrams of a given string**

```python
# The anagram of "abc"
["abc",
"acb",
"bac",
"bca",
"cab",
"cba"]
```

=> Given the string “abcd”, we can **stick the “d” in EVERY POSSIBLE SPOT within each anagram of “abc” **


![[Pasted image 20240414162614.png]]


[[How anagram generation works]]

[[The efficiency of anagram generation]]


