---
tags:
  - "#study"
  - "#review"
cssclasses:
  - center-images
---

> [!tip] Why Top-Down?
> 
> Recursion shines when implementing a top-down approach because **going top down offers a new mental strategy** for tackling a problem


- Top-Down thinking allows us to **solve the problem without us even knowing how to solve the problem**

[[The Top-Down thought process]]

