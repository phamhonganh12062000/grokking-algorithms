---
tags:
  - "#study"
  - "#review"
cssclasses:
  - center-images
---

> [!note]
> In the world of Big O Notation, Selection Sort and Bubble Sort are described in *exactly the same way*.


- The efficiency of Selection Sort is $O\left( \frac{n^2}{2} \right)$ but Big O Notation **ignores constants** => For $n$ data elements, there are $n^2$ steps for Selection Sort

> [!important]
> 
> Big O Notation **ignores constants** => Big O Notation never includes regular numbers that are not an exponent

