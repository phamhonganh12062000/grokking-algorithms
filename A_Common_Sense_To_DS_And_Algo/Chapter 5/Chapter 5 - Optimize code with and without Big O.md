
> [!summary] 3-sentence summary
> 
> - Selection sort searches for the lowest value from left to right and stores its index to compare with an even lower value when found.
> - Selection sort is slightly more efficient than bubble sort, but still $O(N^2)$ as we ignore constants in Big O Notation.
> - Big O Notation ignores constants as it only cares about the long-term trajectory of the algorithm’s steps.


[[Selection sort]]

[[Ignoring constants]]

[[Big O Categories]]

[[Optimize code with and without Big O - Exercises]]



