---
tags:
  - "#study"
  - "#review"
cssclasses:
  - center-images
---
1. Prevent potential bugs
2. Provide a new mental model for tackling problems