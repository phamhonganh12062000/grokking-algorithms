---
tags:
  - "#study"
  - "#review"
cssclasses:
  - center-images
---

- A list of elements with three constraints:
	- Data can only be inserted at the **end/top** of the stack
	- Data can be deleted only from the **end/top** of the stack
	- Only the **last** element of the stack can be read
- Similar to a **vertical array**

![[Pasted image 20240320150103.png]]


- **LIFO** (Last In, First Out)

> [!important]
> 
> As the stack does NOT care about what data structure is under the hood, the stack is an example of an **abstract** data type