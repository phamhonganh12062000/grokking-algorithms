---
tags:
  - "#study"
  - "#review"
cssclasses:
  - center-images
---
- Binary search has a time complexity of $O(\log n)$ (increase one step each time the data is doubled)

![[Pasted image 20240117153046.png]]