
> [!summary] 3-sentence summary
> 
> - The Big O notation expresses the answer to the "key question": *If there are $n$ data elements, how many steps will the algorithm take?*
> - Depending on the scenarios, one algorithm could either take $O(1)$ or $O(N)$ to complete.
> - In CS, we use $\log$ instead of $\log_{2}$


> [!info] Describe Big O Notation in mathematical terms
> 
> The Big O Notation describes the **upper bound of the growth rate of a function**, or that *if a function $g(x)$ grows no faster than a function $f(x)$, then $g$ is said to be a member of $O(f)$*


[[Soul of Big O]]

[[An algorithm of the 3rd kind]]

[[Logarithms]]

[[Big O Notation - Exercises]]

