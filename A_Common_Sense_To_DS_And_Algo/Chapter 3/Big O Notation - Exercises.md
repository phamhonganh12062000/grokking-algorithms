---
tags:
  - "#study"
  - "#review"
cssclasses:
  - center-images
---
1. $O(1)$
2. $O(n)$
3. $O(\log n)$
4. $O(n)$
5. $O(1)$