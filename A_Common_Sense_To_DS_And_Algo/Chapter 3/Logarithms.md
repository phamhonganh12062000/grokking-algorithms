---
tags:
  - "#study"
  - "#review"
cssclasses:
  - center-images
---
- Logarithms are the **inverse of exponents**  e.g., $\log_{2}8 = 3$
- In CS we use $\log$ instead of $\log_{2}$ as shorthand