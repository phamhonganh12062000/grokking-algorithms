---
tags:
  - "#study"
  - "#review"
cssclasses:
  - center-images
---
1. The computer hashes the key e.g., BAD = 2 x 1 x 4 = 8
2. The computer looks inside the cell 8 and returns the stored value


> [!warning]
> 
> Each key can exist only once in the hash table

