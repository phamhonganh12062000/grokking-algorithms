---
tags:
  - "#study"
  - "#review"
cssclasses:
  - center-images
---
- Take characters and convert them into numbers -> **Hashing**

> [!important] One criterion for hash functions
> 
> A hash function must convert the same string to the **same** number every single time it is applied.


- For every key-value pair, each value is stored at the **index** of the key, after the key has been hashed.