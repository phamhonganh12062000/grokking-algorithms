---
tags:
  - "#study"
  - "#review"
cssclasses:
  - center-images
---
Definition: Collisions happen when we try to add data to a cell that is already filled

When collision occurs, we can use [[Separate Chaining]]

