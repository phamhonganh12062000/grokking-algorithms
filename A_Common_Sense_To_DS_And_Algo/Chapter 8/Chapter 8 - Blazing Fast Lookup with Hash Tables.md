---
tags:
  - "#study"
  - "#review"
cssclasses:
  - center-images
---

> [!summary] 3-sentence Summary
> 
> - Hash tables are lists of key-value pairs with each key hashed by a function (hash function) to become an index.
> - To avoid collisions (hashing two different keys to the same index), we can store values in an array inside the cell (separate chaining).
> - A good hash table ensures data is evenly distributed across cells while avoiding collisions and memory hogging.

[[What is a hash table]]

[[Collisions]]

[[Making an efficient hash table]]
